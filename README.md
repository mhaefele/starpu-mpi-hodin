# StarPI mpi hodin

This small projects intends to be a mock-up of the Hodin code in its multi nodes version. 
The idea is to use MPI to communicate between nodes, but to rely on StarPU to perform all required communications.
The repository starts from a serial implementation of a simple problem, then evolves through different StarPU implementations up to something that looks like the current implementations in Hodin.
Finally StarPU-MPI is inserted to be able to run on multi nodes.

## Serial implementation

![Serial implementation](figs/serial.svg "Serial implementation")
The little problem we are looking at consists in adding on each value of Matrix B, its correspondent in A with its left and right neighbours.
The code simply initializes matrix A with integers corresponding to the index of each value in the matrix.
Computation is performed using periodic boundary conditions.
Results is output in an ASCII file.

This implementation can be retrieved with tag `serial`

## Naive StarPU implementation

![Naive StarPU implementation](figs/naive_StarPU.svg "Naive StarPU implementation")
In order to get a parallel implementation we use [StarPU](https://starpu.gitlabpages.inria.fr/), a runtime system using a task based programming model.
The domain is split in the x dimension into n sub-domains.
Each sub-domain is then computed by a different task.
Data dependencies require data exchange between sub-domains.
Of course, it would have been more relevant to split the y direction as there would be no need for data exchange, but the point of this mock up is to test this kind of data exchange.

The big difference is that there are no more large arrays for A and B.
They are both split in blocks and allocated in memory as such.
However no explicit overlaps are allocated in this version
This implementation is "naive" in the sense that the task that computes values on a subdomain of B depends on three sub-domains of A. This implies:
* boundaries communications and computations are serialized
* boundaries communications involve the whole neighbouring sub-domains whereas only part of them are really required which increases comm overhead

This implementation can be retrieved with tag `starpu_naive`

## Advanced StarPU implementation

![Advanced StarPU implementation](figs/advanced_StarPU.svg "Advanced StarPU implementation")
In this version A and B matrices are also allocated by blocks like in the previous one, but this time boundaries on the left and the right are also allocated.
This relaxes the constraints of the naive implementation and allows the runtime system to schedule all boundary copies concurrently with block interior computation. 
Once these steps done, the update of the boundaries can be performed using the updated ghosts on the right and the left.

![StarPU data handler](figs/advanced_StarPU_data_handler.svg "StarPU data handler")
The implementation uses several "views" of the same data as shown on the figure. 
The memory really allocated corresponds to the full block including ghosts.
These views are StarPU data handler pointing to portions of this full block memory and are used while submitting tasks.

This implementation can be retrieved with tag t`starpU_advanced`

## Advanced StarPU MPI implementation

![Advanced StarPU MPI implementation](figs/advanced_StarPU_MPI.svg "Advanced StarPU MPI implementation")
This version is very similar to the previous one in term of memory allocation and task submission. 
Now StarPU is replaced by StarPU-MPI and StarPU data handles have to be declared to MPI, typically to the rank that has to do computation on it.
When data dependencies in the task graph require some transfers between MPI ranks, StarPU will perform the transfer via MPI

This implementation can be retrieved with tag `starpu_MPI` with the gather implemented with MPI_Send and MPI_Recv. 
With tag `starpu_MPI_gather`, the gather operation is performed by StarPU.


